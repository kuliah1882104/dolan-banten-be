package main

import (
	"fmt"
	"pocikode/dolan-banten-be/config"
)

func main() {
	viper := config.NewViper()
	log := config.NewLogger(viper)
	db := config.NewDatabase(viper, log)
	validator := config.NewValidator(viper)
	app := config.NewFiber(viper)

	config.Bootstrap(&config.BootstrapConfig{
		DB:       db,
		App:      app,
		Log:      log,
		Validate: validator,
		Config:   viper,
	})

	port := viper.GetInt("PORT")
	err := app.Listen(fmt.Sprintf(":%d", port))
	if err != nil {
		log.Fatalf("Failed to start server: %v", err)
	}
}
