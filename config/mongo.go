package config

import (
	"context"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func NewDatabase(config *viper.Viper, log *logrus.Logger) *mongo.Database {
	opt := options.Client().ApplyURI(config.GetString("DB_URI"))

	client, err := mongo.Connect(context.TODO(), opt)
	if err != nil {
		log.Fatalf("failed to connect database: %v", err)
	}

	if err = client.Ping(context.TODO(), nil); err != nil {
		log.Fatalf("failed to connect database: %v", err)
	}

	db := client.Database(config.GetString("DB_NAME"))

	return db
}
