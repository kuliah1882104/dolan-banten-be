package config

import (
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/mongo"
	"pocikode/dolan-banten-be/handlers"
	"pocikode/dolan-banten-be/repositories"
	"pocikode/dolan-banten-be/route"
)

type BootstrapConfig struct {
	DB       *mongo.Database
	App      *fiber.App
	Log      *logrus.Logger
	Validate *validator.Validate
	Config   *viper.Viper
}

func Bootstrap(config *BootstrapConfig) {
	placeRepository := repositories.NewPlaceRepository(config.Log, config.DB)
	placeHandler := handlers.NewPlaceHandler(placeRepository, config.Log)

	routeConfig := route.Config{
		App:          config.App,
		PlaceHandler: placeHandler,
	}

	routeConfig.Setup()
}
