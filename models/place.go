package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Location struct {
	Address   string  `bson:"address,omitempty" json:"address,omitempty"`
	Latitude  float64 `bson:"latitude,omitempty" json:"latitude,omitempty"`
	Longitude float64 `bson:"longitude,omitempty" json:"longitude,omitempty"`
}

type Place struct {
	ID               primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
	Name             string             `bson:"name,omitempty" validate:"required" json:"name,omitempty"`
	Categories       []string           `bson:"categories,omitempty" validate:"required" json:"categories,omitempty"`
	ShortDescription string             `bson:"short_description,omitempty" validate:"required" json:"short_description,omitempty"`
	Description      string             `bson:"descriptions,omitempty" validate:"required" json:"descriptions,omitempty"`
	Location         Location           `bson:"location,omitempty" validate:"required" json:"location,omitempty"`
	Thumbnail        string             `bson:"thumbnail,omitempty" validate:"required" json:"thumbnail,omitempty"`
	Images           []string           `bson:"images,omitempty" validate:"required" json:"images,omitempty"`
	Price            string             `bson:"price,omitempty" validate:"required" json:"price,omitempty"`
	Rating           float64            `bson:"rating,omitempty" validate:"required" json:"rating,omitempty"`
	CreatedAt        time.Time          `bson:"created_at,omitempty" validate:"required" json:"created_at,omitempty"`
	UpdatedAT        time.Time          `bson:"updated_at,omitempty" validate:"required" json:"updated_at,omitempty"`
}
