package handlers

import (
	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
	"pocikode/dolan-banten-be/repositories"
	"strconv"
)

type PlaceHandler struct {
	Repository *repositories.PlaceRepository
	Log        *logrus.Logger
}

func NewPlaceHandler(repository *repositories.PlaceRepository, logger *logrus.Logger) *PlaceHandler {
	return &PlaceHandler{
		Repository: repository,
		Log:        logger,
	}
}
func (h *PlaceHandler) Index(ctx *fiber.Ctx) error {
	p := ctx.Query("page", "1")
	l := ctx.Query("limit", "10")
	category := ctx.Query("category")
	search := ctx.Query("search")

	page, err := strconv.Atoi(p)
	if err != nil {
		page = 1
	}

	limit, err := strconv.Atoi(l)
	if err != nil {
		limit = 10
	}

	places, err := h.Repository.GetPlaces(ctx.Context(), &repositories.GetPlaceParams{
		Page:     page,
		Limit:    limit,
		Category: category,
		Search:   search,
	})
	if err != nil {
		h.Log.WithError(err).Error("failed to get places")
		return err
	}

	return ctx.JSON(places)
}
