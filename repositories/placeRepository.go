package repositories

import (
	"context"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"pocikode/dolan-banten-be/models"
)

type PlaceRepository struct {
	DB  *mongo.Database
	Log *logrus.Logger
}

type GetPlaceParams struct {
	Page     int
	Limit    int
	Category string
	Search   string
}

func NewPlaceRepository(log *logrus.Logger, db *mongo.Database) *PlaceRepository {
	return &PlaceRepository{
		DB:  db,
		Log: log,
	}
}

func (r PlaceRepository) GetPlaces(ctx context.Context, params *GetPlaceParams) (*Pagination, error) {
	var places []models.Place

	filter := bson.D{}
	if params.Category != "" {
		filter = append(filter, bson.E{Key: "categories", Value: params.Category})
	}

	if params.Search != "" {
		filter = append(filter, bson.E{Key: "name", Value: bson.D{
			{"$regex", primitive.Regex{Pattern: params.Search, Options: "i"}},
		}})
	}

	cursor, err := r.DB.Collection("places").Find(ctx, filter, newPagination(params.Limit, params.Page).getPaginatedOpts())
	if err != nil {
		return nil, err
	}

	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		var place models.Place
		err := cursor.Decode(&place)
		if err != nil {
			r.Log.WithError(err).Error("failed decoding places")
			return nil, err
		}
		places = append(places, place)
	}

	hasPrev := params.Page > 1
	hasNext := params.Limit == len(places)
	return paginate(params.Page, params.Limit, hasPrev, hasNext, places), nil
}
