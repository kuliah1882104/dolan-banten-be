package repositories

import "go.mongodb.org/mongo-driver/mongo/options"

type pagination struct {
	limit int64
	page  int64
}

func newPagination(limit, page int) *pagination {
	return &pagination{limit: int64(limit), page: int64(page)}
}

func (p pagination) getPaginatedOpts() *options.FindOptions {
	l := p.limit
	skip := p.page*p.limit - p.limit
	return &options.FindOptions{Limit: &l, Skip: &skip}
}

type PaginationInfo struct {
	PrevPage    *int `json:"prev_page"`
	CurrentPage *int `json:"current_page"`
	NextPage    *int `json:"next_page"`
	Limit       *int `json:"limit"`
}

type Pagination struct {
	Pagination PaginationInfo `json:"pagination"`
	Data       any            `json:"data"`
}

func paginate(page, limit int, hasPrev, hasNext bool, data any) *Pagination {
	var paginator PaginationInfo
	paginator.PrevPage = nil
	paginator.NextPage = nil
	paginator.CurrentPage = &page
	paginator.Limit = &limit

	if hasPrev {
		prev := page - 1
		paginator.PrevPage = &prev
	}

	if hasNext {
		next := page + 1
		paginator.NextPage = &next
	}

	return &Pagination{
		Pagination: paginator,
		Data:       data,
	}
}
