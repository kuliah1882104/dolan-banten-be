package route

import (
	"github.com/gofiber/fiber/v2"
	"pocikode/dolan-banten-be/handlers"
)

type Config struct {
	App          *fiber.App
	PlaceHandler *handlers.PlaceHandler
}

func (c *Config) Setup() {
	c.SetupApiRoute()
}

func (c *Config) SetupApiRoute() {
	api := c.App.Group("/api")

	api.Get("/places", c.PlaceHandler.Index)
}
